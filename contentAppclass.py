
import webapp

#html de la pagina existente en {content} va el valor del recurso del dict
PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""
#html del page not founf, envia cuando el recurso no está en el  dict
PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

#clase contentapp (clase hija de webapp)
class ContentApp(webapp.webApp):

#dict {recurso: valor}
    content_dict = {'/funciona': "<p>FUNCIONA CORRECTAMENTE</p>", '/': "<p>Recurso barra</p>",
                '/hello': "<p>HELLO!</p>"}

    def parse (self, request):

    #.split (la separacion, maxsplit)[elemento con el que nos quedamos]
        return request.split(' ',2)[1]

    def process (self, resource):

        if resource in self.content_dict:
            content = self.contents[resource]
            page = PAGE.format(content=content)
            msg = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            msg = "404 Not Found"
        return (msg, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)
